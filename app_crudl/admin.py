from django.contrib import admin
from .models import Proceso, Tarea, Agenda, Periodo

# Register your models here.

# -- Se registran los modelos para gestionarlos a través del administrador
admin.site.register(Proceso)
admin.site.register(Tarea)
admin.site.register(Agenda)
admin.site.register(Periodo)