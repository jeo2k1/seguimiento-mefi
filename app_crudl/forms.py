from django import forms
from django.db.models import fields
from django.forms.models import fields_for_model
from .models import Agenda, Periodo, Proceso, Tarea

class PeriodoForm(forms.ModelForm):
    
    class Meta:
        model = Periodo
        fields = ("id", "nombre")
    #    fields = {'__all__'}
    
    def __init__(self,*args,**kwargs):
            super().__init__(*args,**kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    'class':'form-control'
                })
        
class AgendaForm(forms.ModelForm):
    
    class Meta:
        model = Agenda
        fields = ("finalizada",)
    #    fields = {'__all__'}
    
    def __init__(self,*args,**kwargs):
            super().__init__(*args,**kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    'class':'form-control'
                })

            self.fields['finalizada'].widget.attrs.update({
                'class': 'checkbox'
            })
                

# -- Formularios de Procesos y Tareas

class ProcesosForm(forms.ModelForm):

    class Meta:
        model = Proceso
        fields = ('__all__')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })


class TareasForm(forms.ModelForm):

    class Meta:
        model = Tarea
        fields = ('__all__')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            }) 

        self.fields['activa'].widget.attrs.update({
                'class': 'checkbox'
            })
        