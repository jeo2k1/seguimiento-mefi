from django.db import models
from django.db.models.deletion import CASCADE
from django.db.models.fields import CharField
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

# -- PROCESOS A CONTROLAR
class Proceso(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, null=False)
    descripcion = models.CharField(max_length=255, blank=True)

    class Meta:
        ordering = ('nombre', )

    def __str__(self):
        return self.nombre


# -- TAREAS RELACIONADAS A LOS PROCESOS
class Tarea(models.Model):
    id = models.AutoField(primary_key=True)
    id_proceso = models.ForeignKey(Proceso, on_delete=models.CASCADE, related_name='tarea_proceso')
    nombre = models.CharField(max_length=50, null=False)
    descripcion = models.CharField(max_length=250, blank=True, null=True)
    activa = models.BooleanField(default=True)
    prioridad = models.IntegerField(validators=[ MaxValueValidator(10), MinValueValidator(1)])

    def __str__(self):
        return self.nombre 


# -- PERIODOS (MES-AÑO)
class Periodo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, null=False)

    class Meta:
        ordering = ('nombre', )

    def __str__(self):
        return self.nombre


# -- AGENDA DE TAREAS A CONTROLAR
class Agenda(models.Model):
    id = models.AutoField(primary_key=True)
    id_periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE)
    id_tarea = models.ForeignKey(Tarea, on_delete=models.CASCADE)
    finalizada = models.BooleanField(default=False)
    actualizada = models.DateTimeField(auto_now=True)
    
    class Meta:
        ordering = ('id', 'id_periodo')

    def __str__(self):
        return '{} | {} | {} '.format(self.id_periodo, self.id_tarea, self.id)

    