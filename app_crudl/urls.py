from django.urls import path

from app_login.views import Home
from .views import ABMPeriodos, ABMProcesos, ABMTareas, AgendaView, AgregarPeriodos, AgregarProcesos, EditarProcesos, EliminarProcesos, AgregarTareas, EditarAgenda, EditarPeriodos, EliminarAgenda, EliminarPeriodos, Tareasporperiodos, EliminarProcesos, EditarTareas, EliminarTareas, TareasxProcesoView
from django.contrib.auth import views as auth_views
from app_login.views import Home

app_name="app_crudl"
#app_name="abmprocesos"
#app_name="abmptareas"
#app_name="agenda"

urlpatterns = [  
# PERIODOS               
    path('abmperiodos/',ABMPeriodos.as_view(), name='abmperiodos' ),    
    path('agregarperiodos/',AgregarPeriodos.as_view(), name='agregarperiodos' ),        
    path('editarperiodos/<int:pk>',EditarPeriodos.as_view(), name='editarperiodos' ),            
    path('eliminarperiodos/<int:pk>',EliminarPeriodos.as_view(), name='eliminarperiodos' ),   
# PROCESOS                 
    path('abmprocesos/',ABMProcesos.as_view(), name='abmprocesos' ),      
    path('agregarprocesos/',AgregarProcesos.as_view(), name='agregarprocesos' ),
    path('editarprocesos/<int:pk>', EditarProcesos.as_view(), name='editarprocesos'),
    path('eliminarprocesos/<int:pk>', EliminarProcesos.as_view(), name='eliminarprocesos'),     
# TAREAS    
    path('abmtareas/',ABMTareas.as_view(), name='abmtareas' ),  
    path('agregartareas/',AgregarTareas.as_view(), name='agregartareas' ), 
    path('editartareas/<int:pk>', EditarTareas.as_view(), name='editartareas'),
    path('eliminartareas/<int:pk>', EliminarTareas.as_view(), name='eliminartareas'),
    path('tareasxproceso/',TareasxProcesoView.as_view(), name='tareasxproceso' ),      
    
# TAREA POR PERIODOS
    path('tareaporperiodos/',Tareasporperiodos.as_view(), name='tareaporperiodos' ),            
# AGENDA    
    path('agenda/',AgendaView.as_view(), name='agendaview' ),       
    path('editaragenda/<int:pk>',EditarAgenda.as_view(), name='editaragenda' ), 
    path('eliminaragenda/<int:pk>',EliminarAgenda.as_view(), name='eliminaragenda' ),          
#    path('', Home.as_view(), name="index")  
    ]