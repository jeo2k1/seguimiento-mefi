from django.shortcuts import render
from django.views import generic
from django.db import models
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Periodo, Agenda, Proceso, Tarea
from .forms import PeriodoForm, AgendaForm, ProcesosForm, TareasForm
from django.urls import reverse_lazy

# Create your views here.
#PERIODOS
class ABMPeriodos(LoginRequiredMixin, generic.ListView):   
    model = Periodo
    template_name = 'abm_periodos.html'
    context_object_name = 'obj'    
    login_url = 'login:login'
    
class AgregarPeriodos(LoginRequiredMixin, generic.CreateView):   
    model = Periodo
    template_name = 'agregar_periodos.html'
    success_url = '.'     
    login_url = 'login:login'
    form_class = PeriodoForm 
    
class EditarPeriodos(LoginRequiredMixin, generic.UpdateView):   
    model = Periodo
    template_name = 'editar_periodos.html'
    context_object_name = 'obj'      
    success_url = reverse_lazy('app_crudl:abmperiodos')     
    form_class = PeriodoForm        
    login_url = 'login:login'    
    
class EliminarPeriodos(LoginRequiredMixin, generic.DeleteView):   
    model = Periodo
    template_name = 'eliminar_periodos.html'
    context_object_name = 'obj'    
    success_url = reverse_lazy('app_crudl:abmperiodos')       
    login_url = 'login:login'
    form_class = PeriodoForm      
    
#PROCESOS    
class ABMProcesos(LoginRequiredMixin,generic.ListView):   
    model = Proceso
    template_name = 'abm_procesos.html'
    context_object_name = 'obj'
    success_url = '.'
    login_url = 'login:login'  
    form_class =   ProcesosForm
    
class AgregarProcesos(LoginRequiredMixin, generic.CreateView):   
    model = Proceso
    template_name = 'agregar_procesos.html'
    success_url = '.'     
    login_url = 'login:login'
    form_class = ProcesosForm  

class EditarProcesos(LoginRequiredMixin, generic.UpdateView):   
    model = Proceso
    template_name = 'editar_procesos.html'
    context_object_name = 'obj'      
    success_url = reverse_lazy('app_crudl:abmprocesos')     
    form_class = ProcesosForm
    login_url = 'login:login'    
    
class EliminarProcesos(LoginRequiredMixin, generic.DeleteView):   
    model = Proceso
    template_name = 'eliminar_procesos.html'
    context_object_name = 'obj'    
    success_url = reverse_lazy('app_crudl:abmprocesos')       
    login_url = 'login:login'
    form_class = ProcesosForm   



#TAREAS    
class ABMTareas(LoginRequiredMixin,generic.ListView):   
    model = Tarea
    template_name = 'abm_tareas.html'
    success_url = '.'
    login_url = 'login:login'
    context_object_name = 'obj'
    form_class = TareasForm

    
class AgregarTareas(LoginRequiredMixin, generic.CreateView):   
    model = Tarea
    template_name = 'agregar_tareas.html'
    success_url = '.'     
    login_url = 'login:login'
    form_class = TareasForm 

class EditarTareas(LoginRequiredMixin, generic.UpdateView):   
    model = Tarea
    template_name = 'editar_tareas.html'
    context_object_name = 'obj'      
    success_url = reverse_lazy('app_crudl:abmtareas')     
    form_class = TareasForm
    login_url = 'login:login'    
    
class EliminarTareas(LoginRequiredMixin, generic.DeleteView):   
    model = Tarea
    template_name = 'eliminar_tareas.html'
    context_object_name = 'obj'    
    success_url = reverse_lazy('app_crudl:abmtareas')       
    login_url = 'login:login'
    form_class = TareasForm       
    
class TareasxProcesoView(LoginRequiredMixin,generic.ListView):   
    model = Tarea
    template_name = 'home.html'
    login_url = 'login:login'
    context_object_name = 'obj'        
    
# AGENDA    
class AgendaView(LoginRequiredMixin,generic.ListView):   
    model = Agenda
    template_name = 'agenda.html'
    context_object_name = 'obj_agenda'     
    login_url = 'login:login'    
    
class EditarAgenda(LoginRequiredMixin, generic.UpdateView):   
    model = Agenda
    template_name = 'editar_agenda.html'
    context_object_name = 'obj_agenda'      
    success_url = reverse_lazy('app_crudl:agendaview')     
    form_class = AgendaForm        
    login_url = 'login:login'      
    
class EliminarAgenda(LoginRequiredMixin, generic.DeleteView):   
    model = Agenda
    template_name = 'eliminar_agenda.html'
    context_object_name = 'obj'    
    success_url = reverse_lazy('app_crudl:agendaview')       
    login_url = 'login:login'
    form_class = AgendaForm      
    
# TAREAS POR PERIODO    

class Tareasporperiodos(LoginRequiredMixin,generic.TemplateView):   
    model = Periodo
    template_name = 'tareaporperiodos.html'
    success_url = '.'
    login_url = 'login:login'        
