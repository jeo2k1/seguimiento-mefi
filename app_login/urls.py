from django.urls import path

from django.contrib.auth import views as auth_views

from app_login.views import Home

app_name="login"

urlpatterns = [
   
    path('login/',auth_views.LoginView.as_view(template_name='login.html'), name='login' ),    
    path('logout/',auth_views.LogoutView.as_view(template_name='login.html'),name='logout' ),  
    path('', Home.as_view(), name='index')    
    ]