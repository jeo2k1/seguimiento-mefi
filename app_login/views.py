from django.shortcuts import render
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin

from app_crudl.models import Tarea

""""
# Create your views here.
def Home(request):
    return render(request, 'base/home.html')
"""    
"""
class Home(LoginRequiredMixin,generic.TemplateView):   
    template_name = 'home.html'
    login_url = 'login/'
"""

class Home(LoginRequiredMixin,generic.ListView):   
    model = Tarea
    template_name = 'home.html'
    login_url = 'login:login'
    context_object_name = 'obj'   